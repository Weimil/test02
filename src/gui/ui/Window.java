package gui.ui;

import javax.swing.*;
import gui.logic.*;

import java.awt.*;
import java.awt.event.ActionListener;

public class Window extends JFrame {

    private JPanel rootPanel;
    private JPanel buttonPanel;
    private JPanel resultPanel;
    private JTextArea taInfo;
    private JButton bAddNew;
    private JButton bList;
    private JButton bPrintMap;

    public Window() {
        add(rootPanel);
        setTitle("Hello World");
        setSize(new Dimension(600, 400));
        setMinimumSize(new Dimension(500, 300));
        setLocationRelativeTo(this);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        bAddNew.setActionCommand("addNew");
        bList.setActionCommand("list");
        bPrintMap.setActionCommand("printMap");
    }

    public void setLogic(ActionListener logic) {
        JButton[] buttons = {bAddNew, bList, bPrintMap};
        for (JButton button : buttons) {
            button.addActionListener(logic);
        }
    }

    public void list(String text) {
        taInfo.setText(text);
    }
}
