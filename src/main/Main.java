package main;

import gui.logic.WindowLogic;
import gui.ui.Window;

import javax.swing.*;

public class Main {
    public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {

        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

        Window window = new Window();
        WindowLogic logic = new WindowLogic();

        window.setLogic(logic);
        logic.setWindow(window);

        window.setVisible(true);

    }
}