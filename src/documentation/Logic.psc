Algoritmo LogicaPistonBolt
	
	Definir elegir Como Entero;
	Escribir "1 = direccion, 2 = diagonal/recto" Sin Saltar;
	Leer elegir;
	
	Si (elegir = 1 ) Entonces
		
		Definir again1 Como Entero;
		again1 <-- 1;
		
		Repetir
			
			Definir X, Z Como Entero;
			Definir direction Como Caracter;
			
			Escribir "X = " Sin Saltar;
			Leer X;
			
			Escribir "Z = " Sin Saltar;
			Leer Z;
			
			Si ( X < 0 ) Entonces
				Si ( Z < 0 ) Entonces
					direction <- "NW";
				SiNo
					direction <- "SW";
				FinSi	
			SiNo
				Si ( Z < 0 ) Entonces
					direction <- "NE";
				SiNo
					direction <- "SE";
				FinSi	
			FinSi
			
			Escribir direction;
			
			Escribir "1 para continuar, 0 para parar" Sin Saltar;
			Leer again1;
			
		Mientras Que again1 = 1
		
	SiNo
		
	Si (elegir = 2 ) Entonces
		
		Definir again2 Como Entero;
		again2 <-- 1;
		
		Repetir
			
			Definir X, Z Como Entero;
			Definir camino Como Caracter;
			
			Escribir "X = " Sin Saltar;
			Leer X;
			
			Escribir "Z = " Sin Saltar;
			Leer Z;
			
			Si ( X < 0 ) Entonces
				Si ( Z < 0 ) Entonces
					direction <- "NW";
				SiNo
					direction <- "SW";
				FinSi	
			SiNo
				Si ( Z > 0 ) Entonces
					direction <- "NE";
				SiNo
					direction <- "SE";
				FinSi	
			FinSi
			
			Escribir direction;
			
			Escribir "1 para continuar, 0 para parar" Sin Saltar;
			Leer again2;
			
		Mientras Que again2 = 1
		
	FinSi
	
	FinSi

FinAlgoritmo
